package org.bitbucket.befree.cfutils;

import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Implementation of the {@link CompletableFutureScheduledExecutorService}. Each method will wrap the given
 * {@link Runnable} or {@link Callable} with logic to delegate the response to a newly created {@link CompletableFuture}
 * This implementation doesn't use any additional threads to poll for the future to be done. Instead it wraps the
 * given Runnable and Callable objects to complete when they are finished.
 * <p>
 * Calling cancel on the returned {@link CompletableFuture} is passed through to the original {@link Future}
 */
public class CompletableFutureScheduledExecutorServiceImpl implements CompletableFutureScheduledExecutorService {

    private final ScheduledExecutorService scheduledExecutorService;

    /**
     * Creates an instance using a {@link ScheduledExecutorService} that can be created by normal means.
     * For example:
     * {@link Executors#newSingleThreadScheduledExecutor()}
     *
     * @param scheduledExecutorService The underlying service that will be used to do the work
     */
    public CompletableFutureScheduledExecutorServiceImpl(ScheduledExecutorService scheduledExecutorService) {
        this.scheduledExecutorService = scheduledExecutorService;
    }

    @Override
    public ScheduledCompletableFuture<Void> schedule(Runnable command, long delay, TimeUnit unit) {
        return new ScheduledCompletableFuture<>(f ->
                scheduledExecutorService.schedule(() -> {
                    try {
                        command.run();
                        f.complete(null);
                    } catch (final Exception e) {
                        f.completeExceptionally(e);
                    }
                }, delay, unit));
    }

    @Override
    public <V> ScheduledCompletableFuture<V> schedule(Callable<V> callable, long delay, TimeUnit unit) {
        return new ScheduledCompletableFuture<>(f ->
                scheduledExecutorService.schedule(() -> {
                    try {
                        V result = callable.call();
                        f.complete(result);
                        return result;
                    } catch (final Exception e) {
                        f.completeExceptionally(e);
                        throw e;
                    }
                }, delay, unit));
    }

    @Override
    public ScheduledCompletableFuture<?> scheduleAtFixedRate(Runnable command, long initialDelay, long period, TimeUnit unit) {
        return new ScheduledCompletableFuture<>(f ->
                scheduledExecutorService.scheduleAtFixedRate(() -> {
                    try {
                        command.run();
                        f.complete(null);
                    } catch (final Exception e) {
                        f.completeExceptionally(e);
                    }
                }, initialDelay, period, unit));
    }

    @Override
    public ScheduledCompletableFuture<?> scheduleWithFixedDelay(Runnable command, long initialDelay, long delay, TimeUnit unit) {
        return new ScheduledCompletableFuture<>(f ->
                scheduledExecutorService.scheduleWithFixedDelay(() -> {
                    try {
                        command.run();
                        f.complete(null);
                    } catch (final Exception e) {
                        f.completeExceptionally(e);
                    }
                }, initialDelay, delay, unit));
    }
}
