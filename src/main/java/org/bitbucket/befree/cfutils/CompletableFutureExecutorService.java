package org.bitbucket.befree.cfutils;

import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;

/**
 * Similar to the {@link java.util.concurrent.ExecutorService}, but returns {@link java.util.concurrent.CompletableFuture}s
 * instead of {@link java.util.concurrent.Future}s
 */
public interface CompletableFutureExecutorService {

    /**
     * @see java.util.concurrent.ExecutorService#submit(Callable)
     */
    <T> CompletableFuture<T> submit(Callable<T> task);

    /**
     * @see java.util.concurrent.ExecutorService#submit(Runnable, Object)
     */
    <T> CompletableFuture<T> submit(Runnable task, T result);

    /**
     * @see java.util.concurrent.ExecutorService#submit(Runnable)
     */
    CompletableFuture<?> submit(Runnable task);
}
