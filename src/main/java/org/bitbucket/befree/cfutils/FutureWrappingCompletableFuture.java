package org.bitbucket.befree.cfutils;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;
import java.util.function.Function;

/**
 * Allows adding a callback lambda when cancel is called
 */
public final class FutureWrappingCompletableFuture<T> extends CompletableFuture<T> {
    private final Future<?> future;

    /**
     * No arg constructor, first call needs to be setScheduledFuture before running anything else
     */
    public FutureWrappingCompletableFuture(final Function<CompletableFuture<T>, Future<?>> futureSupplier) {
        future = futureSupplier.apply(this);
        if (future == null) {
            throw new IllegalArgumentException("Invalid future");
        }
    }

    /**
     * Will call cancel on the internal future
     */
    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
        boolean result = future.cancel(mayInterruptIfRunning);
        boolean parentResult = super.cancel(mayInterruptIfRunning);
        return result && parentResult;
    }
}