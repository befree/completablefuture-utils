package org.bitbucket.befree.cfutils;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

/**
 * Similar to the {@link java.util.concurrent.ScheduledExecutorService}, but returns {@link ScheduledCompletableFuture}s
 * instead of {@link java.util.concurrent.ScheduledFuture}s
 */
public interface CompletableFutureScheduledExecutorService {

    /**
     * @see java.util.concurrent.ScheduledExecutorService#schedule(Runnable, long, TimeUnit)
     *
     * @return A better future
     */
    ScheduledCompletableFuture<?> schedule(Runnable command, long delay, TimeUnit unit);

    /**
     * @see java.util.concurrent.ScheduledExecutorService#schedule(Callable, long, TimeUnit)
     *
     * @return A better future
     */
    <V> ScheduledCompletableFuture<V> schedule(Callable<V> callable,
                                      long delay, TimeUnit unit);

    /**
     * @see java.util.concurrent.ScheduledExecutorService#scheduleAtFixedRate(Runnable, long, long, TimeUnit)
     *
     * @return A better future
     */
    ScheduledCompletableFuture<?> scheduleAtFixedRate(Runnable command,
                                             long initialDelay,
                                             long period,
                                             TimeUnit unit);

    /**
     * @see java.util.concurrent.ScheduledExecutorService#scheduleWithFixedDelay(Runnable, long, long, TimeUnit)
     *
     * @return A better future
     */
    ScheduledCompletableFuture<?> scheduleWithFixedDelay(Runnable command,
                                                long initialDelay,
                                                long delay,
                                                TimeUnit unit);

}
