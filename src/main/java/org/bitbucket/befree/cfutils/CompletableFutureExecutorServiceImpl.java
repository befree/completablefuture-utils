package org.bitbucket.befree.cfutils;

import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Returns {@link CompletableFuture}s instead of the hard to use {@link Future} objects. This implementation
 * does not require any additional threads.
 */
public class CompletableFutureExecutorServiceImpl implements CompletableFutureExecutorService {

    private final ExecutorService executorService;

    /**
     * Creates an instance using the given {@link ExecutorService} that can be created by normal means.
     * For example:
     * {@link Executors#newSingleThreadExecutor()}
     *
     * @param executorService The underlying service that will be used to do the work
     */
    public CompletableFutureExecutorServiceImpl(ExecutorService executorService) {
        this.executorService = executorService;
    }

    @Override
    public <T> CompletableFuture<T> submit(Callable<T> command) {
        return new FutureWrappingCompletableFuture<>(f ->
                executorService.submit(() -> {
                    try {
                        T result = command.call();
                        f.complete(result);
                        return result;
                    } catch (final Exception e) {
                        f.completeExceptionally(e);
                        throw e;
                    }
                }));
    }

    @Override
    public <T> CompletableFuture<T> submit(Runnable command, T result) {
        return new FutureWrappingCompletableFuture<>(f ->
                executorService.submit(() -> {
                    try {
                        command.run();
                        f.complete(result);
                    } catch (final Exception e) {
                        f.completeExceptionally(e);
                    }
                }));
    }

    @Override
    public CompletableFuture<?> submit(Runnable command) {
        return new FutureWrappingCompletableFuture<>(f ->
                executorService.submit(() -> {
                    try {
                        command.run();
                        f.complete(null);
                    } catch (final Exception e) {
                        f.completeExceptionally(e);
                    }
                }));
    }
}
