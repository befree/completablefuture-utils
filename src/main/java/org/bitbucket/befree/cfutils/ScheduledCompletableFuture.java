package org.bitbucket.befree.cfutils;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Delayed;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import static java.util.concurrent.TimeUnit.NANOSECONDS;

/**
 * Allows adding a callback lambda when cancel is called
 */
public final class ScheduledCompletableFuture<T> extends CompletableFuture<T> implements Delayed {
    private final ScheduledFuture<?> scheduledFuture;

    /**
     * No arg constructor, first call needs to be setScheduledFuture before running anything else
     */
    public ScheduledCompletableFuture(final Function<CompletableFuture<T>, ScheduledFuture<?>> scheduledFutureSupplier) {
        scheduledFuture = scheduledFutureSupplier.apply(this);
        if (scheduledFuture == null) {
            throw new IllegalArgumentException("Invalid scheduledFuture");
        }
    }

    /**
     * Will call cancel on the internal scheduledFuture
     */
    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
        boolean result = scheduledFuture.cancel(mayInterruptIfRunning);
        boolean parentResult = super.cancel(mayInterruptIfRunning);
        return result && parentResult;
    }

    /**
     * Delegates to the internal scheduledFuture
     */
    @Override
    public long getDelay(TimeUnit unit) {
        return scheduledFuture.getDelay(unit);
    }

    @Override
    public int compareTo(Delayed other) {
        if (other == this) // compare zero if same object
            return 0;
        if (other instanceof ScheduledCompletableFuture) {
            ScheduledCompletableFuture x = (ScheduledCompletableFuture) other;
            return scheduledFuture.compareTo(x.scheduledFuture);
        }
        long diff = getDelay(NANOSECONDS) - other.getDelay(NANOSECONDS);
        return (diff < 0) ? -1 : (diff > 0) ? 1 : 0;
    }
}