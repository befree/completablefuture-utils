package org.bitbucket.befree.cfutils;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CompletableFutureExecutorServiceImplTest {

    private CompletableFutureExecutorServiceImpl classUnderTest;

    @Mock
    private ExecutorService mockExecSvc;

    @BeforeEach
    void runBeforeEach() {
        classUnderTest = new CompletableFutureExecutorServiceImpl(mockExecSvc);
    }

    @Test
    void testSubmitWithRunnable() throws Exception {
        AtomicBoolean hasRun = new AtomicBoolean(false);

        Future mockFuture = mock(Future.class);
        doReturn(mockFuture).when(mockExecSvc).submit(any(Runnable.class));

        // run the method under test
        CompletableFuture<?> completableFuture = classUnderTest.submit(
                () -> hasRun.set(true));

        // capture the Runnable passed through
        ArgumentCaptor<Runnable> runnableArgumentCaptor = ArgumentCaptor.forClass(Runnable.class);
        verify(mockExecSvc).submit(runnableArgumentCaptor.capture());

        assertFalse(completableFuture.isDone(), "future shouldn't be done yet");
        assertFalse(hasRun.get(), "runnable shouldn't be done yet");

        // run the Runnable we captured earlier
        runnableArgumentCaptor.getValue().run();

        // verify everything is done now, the get will timeout if it wasn't completed
        assertTrue(completableFuture.isDone(), "future should be done now");
        completableFuture.get(1, TimeUnit.MILLISECONDS);
        assertTrue(hasRun.get(), "runnable should be done now");
    }

    @Test
    void testSubmitWithRunnableAndResultArguments() throws Exception {

        // used to verify that the Runnable was successfully run
        final AtomicBoolean hasRun = new AtomicBoolean(false);

        final String result = "result";

        // In this case, the implementation doesn't actually call the ExecutableService#submit(Runnable, T) method
        // instead it just passes the result back itself
        final Future futureToReturn = CompletableFuture.completedFuture(result);
        doReturn(futureToReturn).when(mockExecSvc).submit(any(Runnable.class));

        // run the method under test
        final CompletableFuture<String> completableFuture = classUnderTest.submit(() -> hasRun.set(true), result);

        // capture the Runnable
        final ArgumentCaptor<Runnable> runnableArgumentCaptor = ArgumentCaptor.forClass(Runnable.class);
        verify(mockExecSvc).submit(runnableArgumentCaptor.capture());

        assertFalse(completableFuture.isDone(), "future shouldn't be done yet");
        assertFalse(hasRun.get(), "runnable shouldn't be done yet");

        // run the Runnable we captured earlier
        runnableArgumentCaptor.getValue().run();

        // verify everything is done now, the get will timeout if it wasn't completed
        assertTrue(completableFuture.isDone(), "future should be done now");
        String finishedResult = completableFuture.get(1, TimeUnit.MILLISECONDS);
        assertEquals(result, finishedResult, "The finished result should be the result passed into the submit method");
        assertTrue(hasRun.get(), "runnable should be done now");
    }

    @Test
    void testCancelingSubmitWithRunnable() {
        AtomicBoolean hasRun = new AtomicBoolean(false);

        Future mockFuture = mock(Future.class);
        when(mockFuture.cancel(true)).thenReturn(true);
        doReturn(mockFuture).when(mockExecSvc).submit(any(Runnable.class));

        // run the method under test
        CompletableFuture<?> completableFuture = classUnderTest.submit(
                () -> hasRun.set(true));

        // capture the Runnable
        ArgumentCaptor<Runnable> runnableArgumentCaptor = ArgumentCaptor.forClass(Runnable.class);
        verify(mockExecSvc).submit(runnableArgumentCaptor.capture());

        assertFalse(completableFuture.isDone(), "future shouldn't be done yet");
        assertFalse(hasRun.get(), "runnable shouldn't be done yet");

        // now cancel the CompletableFuture
        assertTrue(completableFuture.cancel(true), "cancel should have been successful");

        // verify that the cancel trickles down
        verify(mockFuture).cancel(true);

        // verify everything is done now, the get will timeout if it wasn't completed
        assertTrue(completableFuture.isCancelled(), "future should have been cancelled");
        assertTrue(completableFuture.isDone(), "future should be done now");
        assertThrows(CancellationException.class, () -> completableFuture.get(1, TimeUnit.MILLISECONDS));
        assertFalse(hasRun.get(), "runnable still shouldn't have run");
    }

    @Test
    void testSubmitWithRunnableThatHasException() {
        Future mockFuture = mock(Future.class);
        doReturn(mockFuture).when(mockExecSvc).submit(any(Runnable.class));

        // run the method under test
        CompletableFuture<?> completableFuture = classUnderTest.submit((Runnable) () -> {
            throw new IllegalStateException("test");
        });

        // capture the Runnable
        ArgumentCaptor<Runnable> runnableArgumentCaptor = ArgumentCaptor.forClass(Runnable.class);
        verify(mockExecSvc).submit(runnableArgumentCaptor.capture());

        assertFalse(completableFuture.isDone(), "future shouldn't be done yet");

        // run the runnable we captured earlier, which shouldn't throw any exceptions
        runnableArgumentCaptor.getValue().run();

        // verify is done now, the get will timeout if it wasn't completed
        assertTrue(completableFuture.isDone(), "future should be done now");
        assertTrue(completableFuture.isCompletedExceptionally(), "future have completed exceptionally");

        try {
            completableFuture.get(1, TimeUnit.MILLISECONDS);
            fail("Should have thrown an exception");
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            assertEquals(ExecutionException.class, e.getClass(), "Not expected exception type");
            assertEquals(IllegalStateException.class, e.getCause().getClass(), "Cause not the expected type");
        }
    }

    @Test
    void testSubmitWithCallable() throws Exception {
        AtomicBoolean hasRun = new AtomicBoolean(false);
        String expectedResult = "Test";

        Future mockFuture = mock(Future.class);
        doReturn(mockFuture).when(mockExecSvc).submit(any(Callable.class));

        // run the method under test
        CompletableFuture<String> completableFuture = classUnderTest.submit(() -> {
            hasRun.set(true);
            return expectedResult;
        });

        // capture the Runnable
        ArgumentCaptor<Callable<String>> callableArgumentCaptor = ArgumentCaptor.forClass(Callable.class);
        verify(mockExecSvc).submit(callableArgumentCaptor.capture());

        assertFalse(completableFuture.isDone(), "future shouldn't be done yet");
        assertFalse(hasRun.get(), "callable shouldn't be done yet");

        // run the Runnable we captured earlier
        callableArgumentCaptor.getValue().call();

        // verify everything is done now, the get will timeout if it wasn't completed
        assertTrue(completableFuture.isDone(), "future should be done now");
        assertFalse(completableFuture.isCancelled(), "shouldn't have canceled");
        assertFalse(completableFuture.isCompletedExceptionally(), "shouldn't complete exceptionally");
        assertEquals(expectedResult, completableFuture.get(1, TimeUnit.MILLISECONDS), "not the expected result");
        assertTrue(hasRun.get(), "runnable should be done now");
    }

    @Test
    void testSubmitWithCallableThatHasException() {
        Future mockFuture = mock(Future.class);
        doReturn(mockFuture).when(mockExecSvc).submit(any(Callable.class));

        // run the method under test
        CompletableFuture<String> completableFuture = classUnderTest.submit(() -> {
            throw new IllegalStateException("test");
        });

        // capture the Runnable
        ArgumentCaptor<Callable<String>> callableArgumentCaptor = ArgumentCaptor.forClass(Callable.class);
        verify(mockExecSvc).submit(callableArgumentCaptor.capture());

        assertFalse(completableFuture.isDone(), "future shouldn't be done yet");

        // run the runnable we captured earlier, which shouldn't throw any exceptions
        assertThrows(IllegalStateException.class, () -> {
            callableArgumentCaptor.getValue().call();
        });

        // verify is done now, the get will timeout if it wasn't completed
        assertTrue(completableFuture.isDone(), "future should be done now");
        assertTrue(completableFuture.isCompletedExceptionally(), "future have completed exceptionally");

        try {
            completableFuture.get(1, TimeUnit.MILLISECONDS);
            fail("Should have thrown an exception");
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            assertEquals(ExecutionException.class, e.getClass(), "Not expected exception type");
            assertEquals(IllegalStateException.class, e.getCause().getClass(), "Cause not the expected type");
        }
    }
}