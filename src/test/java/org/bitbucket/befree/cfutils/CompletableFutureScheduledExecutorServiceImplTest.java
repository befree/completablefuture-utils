package org.bitbucket.befree.cfutils;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CompletableFutureScheduledExecutorServiceImplTest {

    private static final long SCHEDULE_DELAY_MS = 123L;
    private static final long SCHEDULE_PERIOD_MS = 456L;
    private static final long SCHED_FUTURE_DELAY = 5L;

    private CompletableFutureScheduledExecutorServiceImpl classUnderTest;

    @Mock
    private ScheduledExecutorService mockSchedExecSvc;

    @BeforeEach
    void runBeforeEach() {
        classUnderTest = new CompletableFutureScheduledExecutorServiceImpl(mockSchedExecSvc);
    }

    @Test
    void testScheduleWithRunnable() throws Exception {
        AtomicBoolean hasRun = new AtomicBoolean(false);

        ScheduledFuture mockSchedFuture = mock(ScheduledFuture.class);
        when(mockSchedFuture.getDelay(TimeUnit.SECONDS)).thenReturn(SCHED_FUTURE_DELAY);
        doReturn(mockSchedFuture).when(mockSchedExecSvc).schedule(any(Runnable.class), eq(SCHEDULE_DELAY_MS), eq(TimeUnit.MILLISECONDS));

        // run the method under test
        ScheduledCompletableFuture<Void> completableFuture = classUnderTest.schedule(
                () -> hasRun.set(true),
                SCHEDULE_DELAY_MS, TimeUnit.MILLISECONDS);

        // verify the delay is coming through
        assertEquals(SCHED_FUTURE_DELAY, completableFuture.getDelay(TimeUnit.SECONDS));

        // capture the Runnable
        ArgumentCaptor<Runnable> runnableArgumentCaptor = ArgumentCaptor.forClass(Runnable.class);
        verify(mockSchedExecSvc).schedule(runnableArgumentCaptor.capture(), eq(SCHEDULE_DELAY_MS), eq(TimeUnit.MILLISECONDS));

        assertFalse(completableFuture.isDone(), "future shouldn't be done yet");
        assertFalse(hasRun.get(), "runnable shouldn't be done yet");

        // run the Runnable we captured earlier
        runnableArgumentCaptor.getValue().run();

        // verify everything is done now, the get will timeout if it wasn't completed
        assertTrue(completableFuture.isDone(), "future should be done now");
        completableFuture.get(1, TimeUnit.MILLISECONDS);
        assertTrue(hasRun.get(), "runnable should be done now");
    }

    @Test
    void testCancelingScheduleWithRunnable() {
        AtomicBoolean hasRun = new AtomicBoolean(false);

        ScheduledFuture mockSchedFuture = mock(ScheduledFuture.class);
        when(mockSchedFuture.cancel(true)).thenReturn(true);
        doReturn(mockSchedFuture).when(mockSchedExecSvc).schedule(any(Runnable.class), eq(SCHEDULE_DELAY_MS), eq(TimeUnit.MILLISECONDS));

        // run the method under test
        ScheduledCompletableFuture<Void> completableFuture = classUnderTest.schedule(
                () -> hasRun.set(true),
                SCHEDULE_DELAY_MS, TimeUnit.MILLISECONDS);

        // capture the Runnable
        ArgumentCaptor<Runnable> runnableArgumentCaptor = ArgumentCaptor.forClass(Runnable.class);
        verify(mockSchedExecSvc).schedule(runnableArgumentCaptor.capture(), eq(SCHEDULE_DELAY_MS), eq(TimeUnit.MILLISECONDS));

        assertFalse(completableFuture.isDone(), "future shouldn't be done yet");
        assertFalse(hasRun.get(), "runnable shouldn't be done yet");

        // now cancel the CompletableFuture
        assertTrue(completableFuture.cancel(true), "cancel should have been successful");

        // verify that the cancel trickles down
        verify(mockSchedFuture).cancel(true);

        // verify everything is done now, the get will timeout if it wasn't completed
        assertTrue(completableFuture.isCancelled(), "future should have been cancelled");
        assertTrue(completableFuture.isDone(), "future should be done now");
        assertThrows(CancellationException.class, () -> completableFuture.get(1, TimeUnit.MILLISECONDS));
        assertFalse(hasRun.get(), "runnable still shouldn't have run");
    }

    @Test
    void testScheduleWithRunnableThatHasError() {
        ScheduledFuture mockSchedFuture = mock(ScheduledFuture.class);
        doReturn(mockSchedFuture).when(mockSchedExecSvc).schedule(any(Runnable.class), eq(SCHEDULE_DELAY_MS), eq(TimeUnit.MILLISECONDS));

        // run the method under test
        ScheduledCompletableFuture<Void> completableFuture = classUnderTest.schedule((Runnable) () -> {
            throw new IllegalStateException("test");
        }, SCHEDULE_DELAY_MS, TimeUnit.MILLISECONDS);

        // capture the Runnable
        ArgumentCaptor<Runnable> runnableArgumentCaptor = ArgumentCaptor.forClass(Runnable.class);
        verify(mockSchedExecSvc).schedule(runnableArgumentCaptor.capture(), eq(SCHEDULE_DELAY_MS), eq(TimeUnit.MILLISECONDS));

        assertFalse(completableFuture.isDone(), "future shouldn't be done yet");

        // run the runnable we captured earlier, which shouldn't throw any exceptions
        runnableArgumentCaptor.getValue().run();

        // verify is done now, the get will timeout if it wasn't completed
        assertTrue(completableFuture.isDone(), "future should be done now");
        assertTrue(completableFuture.isCompletedExceptionally(), "future have completed exceptionally");

        try {
            completableFuture.get(1, TimeUnit.MILLISECONDS);
            fail("Should have thrown an exception");
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            assertEquals(ExecutionException.class, e.getClass(), "Not expected exception type");
            assertEquals(IllegalStateException.class, e.getCause().getClass(), "Cause not the expected type");
        }
    }

    @Test
    void testScheduleWithCallable() throws Exception {
        AtomicBoolean hasRun = new AtomicBoolean(false);
        String expectedResult = "Test";

        ScheduledFuture mockSchedFuture = mock(ScheduledFuture.class);
        when(mockSchedFuture.getDelay(TimeUnit.SECONDS)).thenReturn(SCHED_FUTURE_DELAY);
        doReturn(mockSchedFuture).when(mockSchedExecSvc).schedule(any(Callable.class), eq(SCHEDULE_DELAY_MS), eq(TimeUnit.MILLISECONDS));

        // run the method under test
        ScheduledCompletableFuture<String> completableFuture = classUnderTest.schedule(() -> {
            hasRun.set(true);
            return expectedResult;
        }, SCHEDULE_DELAY_MS, TimeUnit.MILLISECONDS);

        // verify the delay is coming through
        assertEquals(SCHED_FUTURE_DELAY, completableFuture.getDelay(TimeUnit.SECONDS));

        // capture the Runnable
        ArgumentCaptor<Callable<String>> callableArgumentCaptor = ArgumentCaptor.forClass(Callable.class);
        verify(mockSchedExecSvc).schedule(callableArgumentCaptor.capture(), eq(SCHEDULE_DELAY_MS), eq(TimeUnit.MILLISECONDS));

        assertFalse(completableFuture.isDone(), "future shouldn't be done yet");
        assertFalse(hasRun.get(), "callable shouldn't be done yet");

        // run the Runnable we captured earlier
        callableArgumentCaptor.getValue().call();

        // verify everything is done now, the get will timeout if it wasn't completed
        assertTrue(completableFuture.isDone(), "future should be done now");
        assertFalse(completableFuture.isCancelled(), "shouldn't have canceled");
        assertFalse(completableFuture.isCompletedExceptionally(), "shouldn't complete exceptionally");
        assertEquals(expectedResult, completableFuture.get(1, TimeUnit.MILLISECONDS), "not the expected result");
        assertTrue(hasRun.get(), "runnable should be done now");
    }

    @Test
    void testScheduleWithCallableThatHasException() {
        ScheduledFuture mockSchedFuture = mock(ScheduledFuture.class);
        doReturn(mockSchedFuture).when(mockSchedExecSvc).schedule(any(Callable.class), eq(SCHEDULE_DELAY_MS), eq(TimeUnit.MILLISECONDS));

        // run the method under test
        ScheduledCompletableFuture<String> completableFuture = classUnderTest.schedule(() -> {
            throw new IllegalStateException("test");
        }, SCHEDULE_DELAY_MS, TimeUnit.MILLISECONDS);

        // capture the Runnable
        ArgumentCaptor<Callable<String>> callableArgumentCaptor = ArgumentCaptor.forClass(Callable.class);
        verify(mockSchedExecSvc).schedule(callableArgumentCaptor.capture(), eq(SCHEDULE_DELAY_MS), eq(TimeUnit.MILLISECONDS));

        assertFalse(completableFuture.isDone(), "future shouldn't be done yet");

        // run the runnable we captured earlier, which shouldn't throw any exceptions
        assertThrows(IllegalStateException.class, () -> {
            callableArgumentCaptor.getValue().call();
        });

        // verify is done now, the get will timeout if it wasn't completed
        assertTrue(completableFuture.isDone(), "future should be done now");
        assertTrue(completableFuture.isCompletedExceptionally(), "future have completed exceptionally");

        try {
            completableFuture.get(1, TimeUnit.MILLISECONDS);
            fail("Should have thrown an exception");
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            assertEquals(ExecutionException.class, e.getClass(), "Not expected exception type");
            assertEquals(IllegalStateException.class, e.getCause().getClass(), "Cause not the expected type");
        }
    }

    @Test
    void scheduleAtFixedRateWithRunnableThatHasException() {
        AtomicBoolean hasRun = new AtomicBoolean(false);

        ScheduledFuture mockSchedFuture = mock(ScheduledFuture.class);
        when(mockSchedFuture.getDelay(TimeUnit.SECONDS)).thenReturn(SCHED_FUTURE_DELAY);
        doReturn(mockSchedFuture).when(mockSchedExecSvc).scheduleAtFixedRate(any(Runnable.class),
                eq(SCHEDULE_DELAY_MS), eq(SCHEDULE_PERIOD_MS), eq(TimeUnit.MILLISECONDS));

        // run the method under test
        ScheduledCompletableFuture<?> completableFuture = classUnderTest.scheduleAtFixedRate(() -> hasRun.set(true),
                SCHEDULE_DELAY_MS, SCHEDULE_PERIOD_MS, TimeUnit.MILLISECONDS);

        // verify the delay is coming through
        assertEquals(SCHED_FUTURE_DELAY, completableFuture.getDelay(TimeUnit.SECONDS));

        // capture the Runnable
        ArgumentCaptor<Runnable> runnableArgumentCaptor = ArgumentCaptor.forClass(Runnable.class);
        verify(mockSchedExecSvc).scheduleAtFixedRate(runnableArgumentCaptor.capture(), eq(SCHEDULE_DELAY_MS),
                eq(SCHEDULE_PERIOD_MS), eq(TimeUnit.MILLISECONDS));

        assertFalse(completableFuture.isDone(), "future shouldn't be done yet");
        assertFalse(hasRun.get(), "callable shouldn't be done yet");

        // pretend we're the executor and run the Runnable we captured earlier
        runnableArgumentCaptor.getValue().run();

        // verify everything is done now, the get will timeout if it wasn't completed
        assertTrue(completableFuture.isDone(), "future should be done now");
        assertFalse(completableFuture.isCancelled(), "shouldn't have canceled");
        assertFalse(completableFuture.isCompletedExceptionally(), "shouldn't complete exceptionally");
        assertTrue(hasRun.get(), "runnable should be done now");
    }

    @Test
    void scheduleWithFixedDelay() {
    }
}