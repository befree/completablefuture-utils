package org.bitbucket.befree.cfutils;

import org.junit.jupiter.api.Test;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

class CompletableFutureExecutorServiceImplExample {
    @Test
    void exampleSubmitRunnable() throws InterruptedException, ExecutionException, TimeoutException {
        System.out.println("\n\n===== exampleSubmitRunnable =====");

        // Create an ExecutorService like you normally would
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        // wrap it
        CompletableFutureExecutorService completableFutureExecutorService = new CompletableFutureExecutorServiceImpl(executorService);

        // Use the wrapped version to get the lovely CompletableFutures back
        CompletableFuture<?> completableFuture = completableFutureExecutorService.submit(() -> System.out.println("Runnable is running now"));
        System.out.println("Submitted the Runnable for execution");
        completableFuture.whenComplete((v, ex) -> System.out.println("whenComplete works!"));

        // need to wait for the Callable to finish or the test will finish first
        completableFuture.get(100, TimeUnit.MILLISECONDS);
    }

    @Test
    void exampleSubmitCallable() throws InterruptedException, ExecutionException, TimeoutException {
        System.out.println("\n\n===== exampleSubmitCallable =====");

        // Create an ExecutableService like you normally would
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        // wrap it
        CompletableFutureExecutorService completableFutureExecutorService = new CompletableFutureExecutorServiceImpl(executorService);

        // Use the wrapped service to get the lovely CompletableFuture back
        CompletableFuture<String> completableFuture = completableFutureExecutorService.submit(() -> {
            System.out.println("Callable is running now");
            return "I'm am the callable's return value";
        });
        System.out.println("Submitted the Callable for execution");
        completableFuture.whenComplete((value, exception) -> System.out.println("whenComplete works! value=" + value + ", exception=" + exception));

        // need to wait for the Callable to finish or the test will finish first
        String result = completableFuture.get(100, TimeUnit.MILLISECONDS);

        System.out.println("Result is: " + result);
    }
}
