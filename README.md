# CompletableFuture Utils

## Overview
The Java [ExecutorService](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/concurrent/ExecutorService.html)
and [ScheduledExecutorService](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/concurrent/ScheduledExecutorService.html)
interfaces have a lot of great uses, however
they downside is they were added in JDK 5. At that time, [java.util.Future](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/concurrent/Future.html)
was the the only thing around and
[java.util.concurrent.CompletableFuture](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/concurrent/CompletableFuture.html)
wasn't introduced until Java 8. CompletableFutures are way more powerful
than the basic Future interface and so it is desirable to have versions of the services that return CompletableFutures
instead.


This library creates some wrapper interfaces around the ExecutorService and ScheduledExecutorService that has
an identical method signature except it returns CompletableFutures instead.

**Note:** Unlike other wrapper services, the ones in this library do not spin up any additional threads to poll
the Future to determine when it is finished. 

## Building the jar file
After cloning the repository simply run
```
gradlew jar
```

## Usage

### CompletableFutureExecutorService
Here is an example:
```
  // Create the legacy service like you normally would
  ExecutorService executorService = Executors.newSingleThreadExecutor();
  // Then wrap it with the improved implementation
  CompletableFutureExecutorService completableFutureExecutorService = new CompletableFutureExecutorServiceImpl(executorService);
  
  // no use the wrapped service instead of the legacy one
  CompletableFuture<?> completableFuture = completableFutureExecutorService.submit(() -> System.out.println("Hello World!"));
  System.out.println("Started runnable");
  completableFuture.whenComplete((v, ex) -> System.out.println("whenComplete works!"));
```
This typically will print out:

  ```
  Started runnable
  Hello World!
  whenComplete works!
  ```

Check out the JUnit for more examples: CompletableFutureExecutorServiceImplTest.java

### CompletableFutureScheduledExecutorService

Check out the JUnit for more examples: CompletableFutureScheduledExecutorServiceImplTest.java
